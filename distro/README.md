# D I S T R O

These files are the heart of the zine, and holding the major content that you want to share.  By keeping the stuff here, we can follow the emerging convention for dat zines so if your song gets added to any audio zine library, that library will know how to display your info properly.  In other words, this folder helps you customize your own zine nice and easy, but it also helps others aggregate and share yr work more widely without altering any of your intentions.  Autonomy and Community all around!

## Info.txt
This holds all the relevant info you want included in your single zine, and that you want librarians and others to know.  It follows the [smarkt](https://github.com/jondashkyle/smarkt) conventions for text files.

If you are including your track with this zine, than say 'Included' within the Audio section.  If you are hosting the track somewhere else, then write the URL of where its hosted instead.  I think the cool people are including the music with the zine, but who knows really!

The add'l section can include whatever other info you want, and this can be written as [markdown](https://guides.github.com/features/mastering-markdown/), and then rendered in pretty html.  So if you want to include links to your other sites and zines you'd do so with something like:
```
hey I also have other stuff,  [Check out My Cool Link](dat://coollink.fun).
```

## Music/
The music folder is meant to hold a single track, as this is meant as a single track zine!  IT likely already has one in it from whoever you copied this from.  You can right click on it within beaker and choose 'Delete'.  Then you can just drag and drop yr track into here.  You don't need to change the title or anything like that.  As long as you wrote 'Included' in the audio section of  your info.txt file, then we know to look here and find the track no matter what sorta file it is.

## Cover.jpg|gif|png
This is the cover for when it's shared in zine libraries.  It is separate from the background of your zine, which is held in yr aesthetic folder.  In this way, you can have a nice cover for when it's part of a larger collection, and a pretty background for when someone is listening to yr stuff directly.  (It could be the same image, if that's yr aesthetic, but it should still be saved in both of these folders).

The cover can be any sorta image format, and so it could be a good-ass gif if you want.  We are in the future, nothing holds you back.
