const html = require('nanohtml')

module.exports = (state, emit) => {
  if (state.set.name && state.openingVisible) {
    return html`
    <div id='openingInfo'>
    <h1>${state.set.name}</h1>
    <p>${state.set.DJ}</p>
    </div>
  `
  }
}
