const Nanocomponent = require('nanocomponent')
const nanostate = require('nanostate')
const html = require('nanohtml')

module.exports = class PlayerButton extends Nanocomponent {
  constructor (name,state, emit) {
    super(name,state, emit)
    this.state = {
      player: document.querySelector('#player'),
      currentTime: "0'00",
      duration: "0'00"
    }

this.machine = nanostate('off', {
  off: { click: 'playing'},
  playing: { click: 'paused', songDone: 'off'},
  paused: {click: 'playing'},
})
this.machine.on('playing', () => player.play())
this.machine.on('paused', () => player.pause())
}

createElement (state) {
    var set = state.set.Audio
    // setup the svg's, from fontAwesome 
    var playIcon = 'M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z'
    var pauseIcon = 'M144 479H48c-26.5 0-48-21.5-48-48V79c0-26.5 21.5-48 48-48h96c26.5 0 48 21.5 48 48v352c0 26.5-21.5 48-48 48zm304-48V79c0-26.5-21.5-48-48-48h-96c-26.5 0-48 21.5-48 48v352c0 26.5 21.5 48 48 48h96c26.5 0 48-21.5 48-48z'

    var buttonIcon = {
      off: playIcon,
      playing: pauseIcon,
      paused: playIcon
    }[this.machine.state]

    return html`
            <div id='Player'>
            <div id='player-button' class='luminescent' onclick=${() => this.togglePlay()} > 
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path id='icon' d=${buttonIcon}/></svg>
            </div>
            <audio id='player' 
               src='distro/music/${set}' 
               oncanplaythrough=${(e)=> this.updateTime(e, 'duration')}
               ontimeupdate=${(e) => this.updateTime(e, 'currentTime')}
               onended=${() => this.toggleOff()} >
                 It looks like yr browser does not support audio
            </audio>
            <p id='time'>
              <span id='currentTime'>${this.state.currentTime}</span>/
              <span id='duration'>${this.state.duration}</span>
            </p>
           </div>
`
  }

updateTime (e, id) {
  var time = minutesAndSeconds(e.target[id])
  var target = document.querySelector(`#${id}`)
  target.textContent = time
  this.state[target] = time
}

togglePlay () {
  this.machine.emit('click')
  this.rerender()
}

toggleOff () {
  this.machine.emit('songDone')
  this.rerender()
}

update (newTime) {
  return newTime !== this.duration
}
}

function minutesAndSeconds (time) {
  var minutes = Math.floor(time / 60)
  var seconds = Math.floor(time) - minutes * 60
  if (seconds < 10) seconds = `0${seconds}`
  return `${minutes}'${seconds}`
}
