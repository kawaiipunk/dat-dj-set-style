# A E S T H E T I C 

This folder handles all the styling for your page.  There's two stylesheets you can customize, depending on how much you wanna _get into it_.

In either case, you can customize them by clicking on the file above, then clicking the edit pencil in the top right.  This'll get you into beaker's text editor, where you can text edit away to yr heart's content.  Then press save and yr good.

It's handy to have your site open in another tab while doing this. Even handier is to click the three dots in the address bar of your site, and then choosing 'toggle live reloading'.  This will let you see your style changes in real time.

## colors-and-fonts.css

This is the starting styler.  It's likely the only one you need to change.  It lists all the colors and fonts we use for each part of the site.  You can change the background image here, or don't include the image at all and just have a nice gradient.  IT's up to you!  We are using proper CSS conventions for all of these, so this is a good intro to CSS.  It also means that whatever tricks you learn at [mdn.io](https://developer.mozilla.org/en-US/docs/Web/HTML/Applying_color) will be useful here.

## main.css
This holds all the rest of the CSS, determining the actual size and positioning and shape of every element of your zine.  I tried to keep the names for each section logical.  i.e., if you want to change the appearance of the box of information that pops up when you click the question mark, you'd go to the infoBox section.  If you wanted to change the appearance of the question mark, you'd go to the question section.  If this isn't written as intuitively as it could be, let me know!

We are not using any other type of library or tool for htis, it's just basic, dependable CSS.  This means that anything you learn from the mdn docs up above or from [css-tricks.com](https://css-tricks.com/guides/beginner/) will be useful here.  I recommend both sites!
